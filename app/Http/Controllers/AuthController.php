<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $nama = $request['first'];
        $alamat = $request['alamat'];

        return view('welcome', compact('nama'));
    }
}
