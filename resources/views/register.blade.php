<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br> <br>
        <input type="text" name="first"> <br> <br>
        <label>Last name:</label> <br> <br>
        <input type="text" name="last"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="Radio" name="MALE"> Male <br>
        <input type="Radio" name="FEMALE"> Female <br>
        <input type="Radio" name="Other"> Other <br> <br>
        <label> Nationality: </label> <br> <br>
        <select name="National">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
        </select> <br> <br>
        <label>Languange Spoken:</label> <br> <br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea name="alamat" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">

    </form>
    

</body>
</html>